package pao;
import java.util.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


import java.time.LocalDateTime;
public class Main {
	public static void main (String[] args) {
	
		LocalDateTime azi = LocalDateTime.of(2020, 4, 30, 20, 00);
		LocalDateTime maine = LocalDateTime.of(2020, 5, 1, 20, 30);
		
		pao.Spectacol S1 = new pao.Spectacol("Reflection", "Cristina-Andreea Ion", azi, 27.50);
		pao.Spectacol S2 = new pao.Spectacol("Motke Hotul", "Andrei Munteanu", maine, 38.30);
		
		pao.LocLoja L1 = new pao.LocLoja("D", 4, false);
		pao.LocCateg1 L2 = new pao.LocCateg1("A", 12, false);
		pao.LocCateg2 L3 = new pao.LocCateg2("F", 9, false);
		
		ClientNormal C1 = new ClientNormal("Andrei Andreescu", "andand@yahoo.com");
		ClientInvitat C2 = new ClientInvitat("Cornel Cornienco", "corcor@gmail.com");
		ClientFanClub C3 = new ClientFanClub("Ion Ionescu", "ioio@gmail.com");
		
		Rezervare R1 = new Rezervare();
		Rezervare R2 = new Rezervare();
		Rezervare R3 = new Rezervare();
		Rezervare R4 = new Rezervare();
		R1.L3C1(S1, C1, L1);
		R2.L1C3(S1, C2, L2);
		R3.L1C3(S2, C2, L2);
		R4.L2C2(S2, C3, L3);

		List<Rezervare> rezervari = new Vector<Rezervare>();
		rezervari.add(R1);
		rezervari.add(R2);
		rezervari.add(R3);
		rezervari.add(R4);

		List<Client> clienti = new Vector<Client>();
		try {
			for(String line : Files.readAllLines(Paths.get("clienti.csv"))){
				clienti.add(new Client(line.split(",")));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		Service.viewClienti(clienti);

		List<Loc> locuri = new Vector<Loc>();
		try {
			for(String line : Files.readAllLines(Paths.get("locuri.csv"))){
				locuri.add(new Loc(line.split(",")));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		Service.viewLocuri(locuri);

		Service.scrieClienti(clienti);
		Service.scrieLocuri(locuri);

		Service.scrieRezervari(rezervari);

		Service.sorteazaClientiAlfabetic(clienti);
	}
}
