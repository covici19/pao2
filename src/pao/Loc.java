package pao;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
public class Loc {
	protected String rand;
	protected int nr;
	protected boolean taken;
	
	Loc(){
		this.rand = "N/A";
		this.nr = 0;
		this.taken = false;
	}
	
	public Loc(String rand, int nr){
		this.rand = rand;
		this.nr = nr;
		//this.taken = taken;
	}
	public Loc(String a, String b){
		this.rand = a;
		this.nr = Integer.parseInt(b);
		//this.taken = taken;
	}
	public Loc(Loc a) {
		rand = a.rand;
		nr = a.nr ;
	}

	public Loc(String[] values)
	{
		rand=values[0];
		nr=Integer.parseInt(values[1]);
	}
	public void setRand(String rand) {
		this.rand = rand;
	}
	
	public String getRand() {
		return rand;
	}
	
	public void setNr(int nr) {
		this.nr = nr;
	}
	
	public int getNr() {
		return nr;
	}
	
	public void setTaken(boolean taken) {
		this.taken = taken;
	}
	
	public boolean getTaken() {
		return taken;
	}
	
	public void print() {
		System.out.println(rand + nr);
	}
	
	public String locString() {
		String nrString = Integer.toString(nr);
		String loc = rand + "," + nrString;
		return loc;
	}
}
