package pao;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
public class Client implements Comparable<Client>{
	protected String nume;
	protected String email;
	
	public Client(){
		nume = "N/A";
		email = "N/A";
	}

	public Client(String nume, String email){
		this.nume = nume;
		this.email = email;
	}
	public Client(Client a) {
		nume = a.nume;
		email = a.email ;
	}

	public Client(String[] values)
	{
		nume=values[0];
		email=values[1];
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	
	public String getNume() {
		return nume;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}

	@Override
	public int compareTo(Client o) {
		int compare = nume.compareTo(o.nume);
		if (compare==0) {
			compare = email.compareTo(o.email);
		}
		return compare;
	}
	
}
