package pao;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.time.LocalDateTime;

public class Spectacol {
	protected String titlu;
	protected String autor;
	protected LocalDateTime data;
	protected String teatru = "Teatrul Mastroianni";
	protected String adresa = "Strada Stramba nr.33";
	protected double pretBaza;
	
	Spectacol(String titlu, String autor, LocalDateTime data, double pretBaza){
		this.titlu = titlu;
		this.autor = autor;
		this.data = data;
		this.pretBaza = pretBaza;
	}
	
	public void print() {
		System.out.println("Spectacolul " + titlu);
		System.out.println("scris de " + autor);
		System.out.println(data.toString());
		System.out.println(teatru);
		System.out.println(adresa);
	}
	
	public double getPretBaza() {
		return pretBaza;
	}
	
	public String getTitlu() {
		return titlu;
	}
	
	public LocalDateTime getData() {
		return data;
	}
}
