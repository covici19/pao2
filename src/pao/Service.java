package pao;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
public class Service {
    public static void log(String action) {
        try {
            FileWriter fw = new FileWriter("logs.csv",true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter pw = new PrintWriter(bw);
            Date date = new Date();
            String dateStr = date.toString();
            pw.println(action+","+ dateStr);
            pw.flush();
            pw.close();
        }
        catch(Exception E)
        {
            JOptionPane.showMessageDialog(null, "Nu s-a putut scrie");
        }
    }
    public static void viewClienti(List<pao.Client> clienti)
    {
        for(Client client: clienti)
            System.out.println(client.getNume()+" "+ client.getEmail());
        log("viewClienti");
    }
    public static void viewLocuri(List<pao.Loc> locuri)
    {
        for(Loc loc: locuri)
            System.out.println(loc.locString());
        log("viewLocuri");
    }
    public static void viewRezervari(List<pao.Rezervare> rezervari)
    {
        for(Rezervare rezervare: rezervari)
            System.out.println(rezervare.rezString());
        log("viewRezervari");
    }
    public static void scrieClienti(List<Client> clienti)
    {
        for(Client client: clienti) {
            try {
                FileWriter fw = new FileWriter("clienti.csv",true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);
                pw.println(client.getNume()+","+ client.getEmail());
                pw.flush();
                pw.close();
            } catch(Exception E)
            {
                JOptionPane.showMessageDialog(null, "Nu s-a putut scrie");
            }}
        log("scrieClienti");
    }
    public static void scrieLocuri(List<pao.Loc> locuri)
    {
        for(Loc loc: locuri) {
            try {
                FileWriter fw = new FileWriter("locuri.csv",true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);
                pw.println(loc.locString());
                pw.flush();
                pw.close();
            } catch(Exception E)
            {
                JOptionPane.showMessageDialog(null, "Nu s-a putut scrie");
            }}
        log("scrieLocuri");
    }
    public static void scrieRezervari(List<pao.Rezervare> rezervari)
    {
        for(Rezervare rezervare: rezervari) {
            try {
                FileWriter fw = new FileWriter("rezervari.csv",true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter pw = new PrintWriter(bw);
                pw.println(rezervare.rezString());
                pw.flush();
                pw.close();
            } catch(Exception E)
            {
                JOptionPane.showMessageDialog(null, "Nu s-a putut scrie");
            }}
        log("scrieRezervari");
    }
    public static void sorteazaClientiAlfabetic(List<Client> clienti)
    {
        Collections.sort(clienti);
        System.out.println(clienti);
        log("sorteazaClientiAlfabetic");
    }
}
