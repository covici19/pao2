package pao;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class Rezervare {
	
	protected int cod;
	List<String[]> dataList = new ArrayList<String[]>();
	
	Rezervare(){
		cod = 0;
	}
	
	Rezervare(int cod){
		this.cod = cod;
	}
	
	//loc(L): 1 - categ1; 2 - categ2; 3 - loja
	//client(C): 1 - normal; 2 - fan club; 3 - invitat
	public String rezString (){
		String rez = "";
		for(String[] data: dataList)
			rez = rez + data[0] + "," + data[1] + "," + data[2] + "," + data[3] + "," + data[4];
		return rez;
	}
	public void L1C1(pao.Spectacol s, pao.ClientNormal c, pao.LocCateg1 l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L1C2(pao.Spectacol s, pao.ClientFanClub c, pao.LocCateg1 l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L1C3(pao.Spectacol s, pao.ClientInvitat c, pao.LocCateg1 l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L2C1(pao.Spectacol s, pao.ClientNormal c, pao.LocCateg2 l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L2C2(pao.Spectacol s, pao.ClientFanClub c, pao.LocCateg2 l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L2C3(pao.Spectacol s, pao.ClientInvitat c, pao.LocCateg2 l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L3C1(pao.Spectacol s, pao.ClientNormal c, pao.LocLoja l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L3C2(pao.Spectacol s, pao.ClientFanClub c, pao.LocLoja l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	public void L3C3(pao.Spectacol s, pao.ClientInvitat c, pao.	LocLoja l) {
		double pret;
		pret = c.pret(l.pret(s.getPretBaza()));
		String pretString = String.valueOf(pret);
		String[] data = {s.getTitlu(), 
						 s.getData().toString(),
						 c.getNume(),
						 l.locString(),
						 pretString};
		dataList.add(data); 
	}
	/*
	public void write(String filePath) 
	{ 
	  
	    File file = new File(filePath);
	  
	    try { 
	        FileWriter outputfile = new FileWriter(file); 
	        CSVWriter writer = new CSVWriter(outputfile); 
	        
	        writer.writeAll(dataList); 
	        writer.close(); 
	    } 
	    catch (IOException e) {  
	        e.printStackTrace(); 
	    } 
	} */
	
}
