package pao;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
public class LocCateg2 extends pao.Loc {
	
	LocCateg2(String rand, int nr, boolean taken){
		this.rand = rand;
		this.nr = nr;
		this.taken = taken;
	}
	
	public double pret (double pretBaza) {
		return pretBaza;
	}
	
	public void print () {
		System.out.println(rand + nr + " (categoria 2)");
	}
}
